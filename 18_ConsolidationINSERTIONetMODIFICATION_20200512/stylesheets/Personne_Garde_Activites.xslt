<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 
	<xsl:output 
			encoding="ISO-8859-15"
			method="xml"
			indent="yes" />
		
 		<xsl:template match="activites">
				<xsl:copy-of select="."/>
		</xsl:template>
		<xsl:template match="text()"/>
	
	</xsl:stylesheet>