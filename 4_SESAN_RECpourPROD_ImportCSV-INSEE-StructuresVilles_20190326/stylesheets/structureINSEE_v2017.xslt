<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="ISO-8859-1" indent="yes" method="xml" version="1.0"/>
	<!--	####################### 	-->	
	<!--	TEMPLATE ROW(RACINE) 		-->
	<!--	####################### 	-->

	<!-- INPUT -->
	<xsl:param name="DATEMAJ" select="DATEMAJ"/>

	<xsl:template match="row">
		<!-- STRUCTURE -->
		<config>
			<xsl:attribute name="etatRubrique">
				<xsl:value-of select="'LECTURE'"/>
			</xsl:attribute>
			<xsl:attribute name="libelle">
				<xsl:value-of select="'IDF_Geographique'"/>
			</xsl:attribute>			
			<structure>
				<xsl:attribute name="etatRubrique">
					<xsl:value-of select="numero_departement/text()"/>
				</xsl:attribute>
				<xsl:attribute name="idRubrique">
					<xsl:value-of select="numero_departement/text()"/>
				</xsl:attribute>	
				<structures>
					<structure>
						<xsl:attribute name="etatRubrique">
							<xsl:value-of select="code_insee/text()"/>
						</xsl:attribute>
						<xsl:attribute name="idRubrique">
							<xsl:value-of select="code_insee/text()"/>
						</xsl:attribute>	
						<libelleComplet>
							<xsl:value-of select="nom_commune/text()"/>
						</libelleComplet>
						<libelleCourt>
							<xsl:value-of select="code_insee/text()"/>
						</libelleCourt>
						<rue>
							<xsl:value-of select="'CENTRE VILLE'"/>
						</rue>		   
						<codePostal>
							<xsl:value-of select="codes_postaux/text()"/>
						</codePostal>
						<nom_departement>
							<code>
								<xsl:value-of select="numero_departement/text()"/>
							</code>
						</nom_departement>
						<nom_ville>
							<code>
								<xsl:value-of select="code_insee/text()"/>
							</code>
						</nom_ville>
						<nom_region>
							<code>
								<xsl:value-of select="'11'"/>
							</code>
						</nom_region>
						<codeUnite>
							<xsl:value-of select="code_insee/text()"/>
						</codeUnite>			
						<structureType>
							<xsl:attribute name="etatRubrique">
								<xsl:value-of select="'LECTURE'"/>
							</xsl:attribute>
							<xsl:attribute name="idRubrique">
								<xsl:value-of select="''"/>
							</xsl:attribute>	
							<code>
								<xsl:value-of select="'3'"/>
							</code>
						</structureType>
<!--
						<extensionsStructure>	
							<extensionStructure>
								<metaDataColonne>
									<nomcolonne>PAYS</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="'99000'"/>
								</valeur>  
							</extensionStructure>						
							<extensionStructure>
								<metaDataColonne>
									<nomcolonne>TYPE_STRUCTURE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="'3'"/>
								</valeur>  
							</extensionStructure>
							<extensionStructure>
								<metaDataColonne>
									<nomcolonne>TYPO_STRUCTURE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="'2'"/>
								</valeur>  
							</extensionStructure>
							<extensionStructure>
								<metaDataColonne>
									<nomcolonne>DATEMAJ_STR</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="$DATEMAJ"/>
								</valeur>  
							</extensionStructure>
						</extensionsStructure>
-->
						<latitude>
							<xsl:value-of select="latitude/text()"/>
						</latitude>
						<longitude>
							<xsl:value-of select="longitude/text()"/>
						</longitude>
					</structure>
				</structures>
			</structure>
		</config>		
	</xsl:template>
</xsl:stylesheet>
