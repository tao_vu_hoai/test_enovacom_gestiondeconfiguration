<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" version="1.0"/>
	<!--	######################### 	-->
	<!--	 TEMPLATE ROW(RACINE) 		-->
	<!--	######################### 	-->
	<!--	#SESAN ENTITE JURIDIQUE	# 	-->
	<!--	#    05 DEC 2018  CCO   # 	-->
	<!--	######################### 	-->
	<!-- apostropheEnovacom -->
	<xsl:variable name="apostropheEnovacom" select="''"/>
	<!-- CONFIG -->
	<xsl:template match="row">
		<config>
			<xsl:attribute name="etatRubrique">
				<xsl:value-of select="'LECTURE'"/>
			</xsl:attribute>
			<xsl:attribute name="libelle">
				<xsl:value-of select="'IDF_Santé'"/>
			</xsl:attribute>
			<!-- STRUCTURE -->
			<structure>
				<xsl:attribute name="etatRubrique">
					<xsl:value-of select="'LECTURE'"/>
				</xsl:attribute>
				<xsl:attribute name="idRubrique">
					<xsl:value-of select="'2'"/>
				</xsl:attribute>
				<structures>
					<structure>
						<xsl:attribute name="etatRubrique">
							<xsl:value-of select="concat('1',nofiness/text())"/>
						</xsl:attribute>
						<xsl:attribute name="idRubrique">
							<xsl:value-of select="concat('1',nofiness/text())"/>
						</xsl:attribute>
						<structureType>
							<xsl:attribute name="etatRubrique">
								<xsl:value-of select="'LECTURE'"/>
							</xsl:attribute>
							<xsl:attribute name="idRubrique">
								<xsl:value-of select="''"/>
							</xsl:attribute>
							<code>
								<xsl:value-of select="'0001'"/>
							</code>
						</structureType>
						<libelleComplet>
							<xsl:value-of select="rs/text()"/>
						</libelleComplet>
						<libelleCourt>
							<xsl:value-of select="concat('1',nofiness/text())"/>
						</libelleCourt>
						<codeUnite>
							<xsl:value-of select="concat('1',nofiness/text())"/>
						</codeUnite>
						<xsl:if test="datecrea/text()!=''">
							<dateOuverture>
								<xsl:value-of select="concat(substring(//datecrea/text(),9,2),'/',substring(//datecrea/text(),6,2),'/',substring(//datecrea/text(),1,4))"/>
							</dateOuverture>
						</xsl:if>
						<xsl:if test="datefermeture/text()!=''">
							<dateFermeture>
								<xsl:value-of select="concat(substring(//datefermeture/text(),9,2),'/',substring(//datefermeture/text(),6,2),'/',substring(//datefermeture/text(),1,4))"/>
							</dateFermeture>
						</xsl:if>
						<extensionsStructure>
							<extensionStructure delete="false">
								<metaDataColonne>
									<nomcolonne>STR_SOURCEALIMENTATION</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="'FINESS'"/>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>TYPEIDNAT_STRUCT</nomcolonne>
								</metaDataColonne>
								<valeur> 					
									<xsl:value-of select="'5'"/>													
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>IDNAT_STRUCT</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="concat('1',nofiness/text())"/>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>RAISONSOCIALEEJ</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="rs/text()!=''">
											<xsl:value-of select="rs/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>NUMSIREN</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="siren/text()!=''">
											<xsl:value-of select="siren/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>NUMFINESSJURI</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="nofiness/text()!=''">
											<xsl:value-of select="nofiness/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>RAISONSOCIALELONGUE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="rslongue/text()!=''">
											<xsl:value-of select="rslongue/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>


							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>COMPLEMENTRAISONSOCIALE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="complrs/text()!=''">
											<xsl:value-of select="complrs/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>


							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>ADRESSEEJ</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="concat((//numvoie/text()),' ',(//typvoie/text()),' ',(//voie/text()),' ',(//ligneacheminement/text()))"/>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_NUMVOIE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="numvoie/text()!=''">
											<xsl:value-of select="numvoie/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_TYPVOIE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="typvoie/text()!=''">
											<xsl:value-of select="typvoie/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_VOIE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="voie/text()!=''">
											<xsl:value-of select="voie/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>



							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_COMPVOIE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="compvoie/text()!=''">
											<xsl:value-of select="compvoie/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>



							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_COMPLDISTRIB</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="compldistrib/text()!=''">
											<xsl:value-of select="compldistrib/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_LIEUDITBP</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="lieuditbp/text()!=''">
											<xsl:value-of select="lieuditbp/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_COMMUNE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="commune/text()!=''">
											<xsl:choose>
												<xsl:when test="departement/text()!=''">
													<xsl:value-of select="concat(departement/text(),commune/text())"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="concat('__',commune/text())"/>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="departement/text()!=''">
													<xsl:value-of select="concat(departement/text(),'___')"/>
												</xsl:when>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_LIGNEACHEMIN</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="ligneacheminement/text()!=''">
											<xsl:value-of select="ligneacheminement/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_LIBCOMMUNE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="libcommune/text()!=''">
											<xsl:value-of select="libcommune/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_DEPARTEMENT</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="departement/text()!=''">
											<xsl:value-of select="departement/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_LIBDEPARTEME</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="libdepartement/text()!=''">
											<xsl:value-of select="libdepartement/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_CODEPOSTAL</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="codepostal/text()!=''">
											<xsl:value-of select="codepostal/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_CODEPAYS</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="codepays/text()!=''">
											<xsl:value-of select="codepays/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_LIBELLEPAYS</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="libellepays/text()!=''">
											<xsl:value-of select="libellepays/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>TELECOMMUNICATION</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:value-of select="concat('Téléphone : ',(//telephone/text()),' - Fax : ',((//telecopie/text())))"/>
								</valeur>
							</extensionStructure>
							<xsl:choose>
								<xsl:when test="categetab/text()!=''">

									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>CATEGORIEETABLISSEMENT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="categetab/text()!=''">
													<xsl:value-of select="categetab/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
								</xsl:when>
							</xsl:choose>

							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>STATUTJURIDIQUE</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="statutjuridique/text()!=''">
											<xsl:value-of select="statutjuridique/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>DATEMODIFICATIONSIREN</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="datemodifsiren/text()!=''">
											<xsl:value-of select="datemodifsiren/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>ORIGINEMODIFICATIONSIREN</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="originemodifsiren/text()!=''">
											<xsl:value-of select="originemodifsiren/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<xsl:choose>
								<xsl:when test="codeape/text()!=''">
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>CODEAPEN</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="codeape/text()!=''">
													<xsl:value-of select="concat(substring(//codeape/text(),1,2),'.',substring(//codeape/text(),3,5))"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
								</xsl:when>
							</xsl:choose>
							<xsl:choose>
								<xsl:when test="typefermeture/text()!=''">
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>TYPEFERMETURE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="typefermeture/text()!=''">
													<xsl:value-of select="typefermeture/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
								</xsl:when>
							</xsl:choose>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>GEO_FINESS_DATEMAJ</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="datemaj/text()!=''">
											<xsl:value-of select="datemaj/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>EJ_FINESS_QUALIFCREATION</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="qualifcreation/text()!=''">
											<xsl:value-of select="qualifcreation/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
							<extensionStructure delete="true">
								<metaDataColonne>
									<nomcolonne>DATECREATION</nomcolonne>
								</metaDataColonne>
								<valeur>
									<xsl:choose>
										<xsl:when test="datecrea/text()!=''">
											<xsl:value-of select="datecrea/text()"/>
										</xsl:when>
									</xsl:choose>
								</valeur>
							</extensionStructure>
						</extensionsStructure>
					</structure>
				</structures>
			</structure>
		</config>
	</xsl:template>
</xsl:stylesheet>