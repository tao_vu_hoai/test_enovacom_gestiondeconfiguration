<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" version="1.0"/>
	<!--	####################### 	-->
	<!--	TEMPLATE ROW(RACINE) 		-->
	<!--	####################### 	-->
	<!--	#SESAN ENTITE GEOGRAPHIQUE	# 	-->
	<!--	#    05 DEC 2018  CCO   # 	-->
	<!--	######################### 	-->
	<xsl:variable name="apostropheEnovacom" select='"&apos;"'/>
	<!-- CONFIG -->
	<xsl:template match="row">
		<config>
			<xsl:attribute name="etatRubrique">
				<xsl:value-of select="'LECTURE'"/>
			</xsl:attribute>
			<xsl:attribute name="libelle">
				<xsl:value-of select="'IDF_Santé'"/>
			</xsl:attribute>
			<!-- STRUCTURE -->
			<structure>
				<xsl:attribute name="etatRubrique">
					<xsl:value-of select="'LECTURE'"/>
				</xsl:attribute>
				<xsl:attribute name="idRubrique">
					<xsl:value-of select="'2'"/>
				</xsl:attribute>
				<structures>
					<structure>
						<xsl:attribute name="etatRubrique">
							<xsl:value-of select="'LECTURE'"/>
						</xsl:attribute>
						<xsl:attribute name="idRubrique">
							<xsl:value-of select="concat('1',nofinessej/text())"/>
						</xsl:attribute>
						<structures>
							<structure>
								<xsl:attribute name="etatRubrique">
									<xsl:value-of select="concat('1',nofinesset/text())"/>
								</xsl:attribute>
								<xsl:attribute name="idRubrique">
									<xsl:value-of select="concat('1',nofinesset/text())"/>
								</xsl:attribute>
								<structureType>
									<xsl:attribute name="etatRubrique">
										<xsl:value-of select="'LECTURE'"/>
									</xsl:attribute>
									<xsl:attribute name="idRubrique">
										<xsl:value-of select="''"/>
									</xsl:attribute>
									<code>
										<xsl:value-of select="'0002'"/>
									</code>
								</structureType>
								<libelleComplet>
									<xsl:value-of select="rs/text()"/>
								</libelleComplet>
								<libelleCourt>
									<xsl:value-of select="concat('1',nofinesset/text())"/>
								</libelleCourt>
								<codeUnite>
									<xsl:value-of select="concat('1',nofinesset/text())"/>
								</codeUnite>
								<xsl:if test="dateouv/text()!=''">
									<dateOuverture>
										<xsl:value-of select="concat(substring(//dateouv/text(),9,2),'/',substring(//dateouv/text(),6,2),'/',substring(//dateouv/text(),1,4))"/>
									</dateOuverture>
								</xsl:if>
								<xsl:if test="datefermeture/text()!=''">
									<dateFermeture>
										<xsl:value-of select="concat(substring(//datefermeture/text(),9,2),'/',substring(//datefermeture/text(),6,2),'/',substring(//datefermeture/text(),1,4))"/>
									</dateFermeture>
								</xsl:if>
								<extensionsStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>STR_SOURCEALIMENTATION</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:value-of select="'FINESS'"/>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>TYPEIDNAT_STRUCT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:value-of select="'6'"/>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>IDNAT_STRUCT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:value-of select="concat('1',nofinesset/text())"/>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>NUMSIRET</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="siret/text()!=''">
													<xsl:value-of select="siret/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>NUMFINESSJURI</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="nofinessej/text()!=''">
													<xsl:value-of select="nofinessej/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>NUMFINESSGEO</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="nofinesset/text()!=''">
													<xsl:value-of select="nofinesset/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>ETABLISSEMENTPRINCIPAL</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="nofinessppal/text()!=''">
													<xsl:value-of select="nofinessppal/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>DENOMINATIONEG</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="rs/text()!=''">
													<xsl:value-of select="rs/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>DENOMINATIONEGLONGUE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="rslongue/text()!=''">
													<xsl:value-of select="rslongue/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>COMPLEMENTDENOMINATIONEG</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="complrs/text()!=''">
													<xsl:value-of select="complrs/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>ADRESSEEG</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:value-of select="concat((//numvoie/text()),' ',(//typvoie/text()),' ',(//voie/text()),' ',(//ligneacheminement/text()))"/>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_NUMVOIE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="numvoie/text()!=''">
													<xsl:value-of select="numvoie/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_TYPVOIE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="typvoie/text()!=''">
													<xsl:value-of select="typvoie/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_VOIE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="voie/text()!=''">
													<xsl:value-of select="voie/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_COMPVOIE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="compvoie/text()!=''">
													<xsl:value-of select="compvoie/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_COMPLDISTRIB</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="compldistrib/text()!=''">
													<xsl:value-of select="compldistrib/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_LIEUDITBP</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="lieuditbp/text()!=''">
													<xsl:value-of select="lieuditbp/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_COMMUNE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="commune/text()!=''">
													<xsl:choose>
														<xsl:when test="departement/text()!=''">
															<xsl:value-of select="concat(departement/text(),commune/text())"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="concat('__',commune/text())"/>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:when>
												<xsl:otherwise>
													<xsl:choose>
														<xsl:when test="departement/text()!=''">
															<xsl:value-of select="concat(departement/text(),'___')"/>
														</xsl:when>
													</xsl:choose>
												</xsl:otherwise>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_LIGNEACHEMIN</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="ligneacheminement/text()!=''">
													<xsl:value-of select="ligneacheminement/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_LIBCOMMUNE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="libcommune/text()!=''">
													<xsl:value-of select="libcommune/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_DEPARTEMENT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="departement/text()!=''">
													<xsl:value-of select="departement/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_LIBDEPARTEME</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="libdepartement/text()!=''">
													<xsl:value-of select="libdepartement/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_CODEPOSTAL</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="codepostal/text()!=''">
													<xsl:value-of select="codepostal/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_CODEPAYS</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="codepays/text()!=''">
													<xsl:value-of select="codepays/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_LIBELLEPAYS</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="libellepays/text()!=''">
													<xsl:value-of select="libellepays/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>TELECOMMUNICATION</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:value-of select="concat('Téléphone : ',(//telephone/text()),' - Fax : ',((//telecopie/text())))"/>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>BOITELETTRESMSS</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="courriel/text()!=''">
													<xsl:value-of select="courriel/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<xsl:choose>
										<xsl:when test="categetab/text()!=''">
											<extensionStructure delete="true">
												<metaDataColonne>
													<nomcolonne>CATEGORIEETABLISSEMENT</nomcolonne>
												</metaDataColonne>
												<valeur>
													<xsl:choose>
														<xsl:when test="categetab/text()!=''">
															<xsl:value-of select="categetab/text()"/>
														</xsl:when>
													</xsl:choose>
												</valeur>
											</extensionStructure>
										</xsl:when>
									</xsl:choose>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>TYPEETABLISSEMENT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="typeet/text()!=''">
													<xsl:value-of select="typeet/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>NATUREETABLISSEMENT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="natureet/text()!=''">
													<xsl:value-of select="natureet/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>DATEMODIFICATIONSIRET</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="datemodifsiret/text()!=''">
													<xsl:value-of select="datemodifsiret/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>ORIGINEMODIFICATIONSIRET</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="originemodifsiret/text()!=''">
													<xsl:value-of select="originemodifsiret/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<xsl:choose>
										<xsl:when test="codeape/text()!=''">
											<extensionStructure delete="true">
												<metaDataColonne>
													<nomcolonne>CODEAPET</nomcolonne>
												</metaDataColonne>
												<valeur>
													<xsl:choose>
														<xsl:when test="codeape/text()!=''">
															<xsl:value-of select="concat(substring(//codeape/text(),1,2),'.',substring(//codeape/text(),3,5))"/>
														</xsl:when>
													</xsl:choose>
												</valeur>
											</extensionStructure>
										</xsl:when>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="codemft/text()!=''">
											<extensionStructure delete="true">
												<metaDataColonne>
													<nomcolonne>MODEFIXATIONTARIF</nomcolonne>
												</metaDataColonne>
												<valeur>
													<xsl:choose>
														<xsl:when test="codemft/text()!=''">
															<xsl:value-of select="codemft/text()"/>
														</xsl:when>
													</xsl:choose>
												</valeur>
											</extensionStructure>
										</xsl:when>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="codesph/text()!=''">
											<extensionStructure delete="true">
												<metaDataColonne>
													<nomcolonne>MODALITEPARTICIPATIONSPH</nomcolonne>
												</metaDataColonne>
												<valeur>
													<xsl:choose>
														<xsl:when test="codesph/text()!=''">
															<xsl:value-of select="codesph/text()"/>
														</xsl:when>
													</xsl:choose>
												</valeur>
											</extensionStructure>
										</xsl:when>
									</xsl:choose>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>DATECADUCITE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="datelimite/text()!=''">
													<xsl:value-of select="datelimite/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<xsl:choose>
										<xsl:when test="indcaduc/text()!=''">
											<extensionStructure delete="true">
												<metaDataColonne>
													<nomcolonne>CONSTATCADUCITE</nomcolonne>
												</metaDataColonne>
												<valeur>
													<xsl:value-of select="indcaduc/text()"/>
												</valeur>
											</extensionStructure>
										</xsl:when>
									</xsl:choose>
									<xsl:choose>
										<xsl:when test="typefermeture/text()!=''">
											<extensionStructure delete="true">
												<metaDataColonne>
													<nomcolonne>TYPEFERMETUREEG</nomcolonne>
												</metaDataColonne>
												<valeur>
													<xsl:choose>
														<xsl:when test="typefermeture/text()!=''">
															<xsl:value-of select="typefermeture/text()"/>
														</xsl:when>
													</xsl:choose>
												</valeur>
											</extensionStructure>
										</xsl:when>
									</xsl:choose>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>DATEAUTORISATION</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="dateautor/text()!=''">
													<xsl:value-of select="dateautor/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>GEO_FINESS_DATEMAJ</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="datemaj/text()!=''">
													<xsl:value-of select="datemaj/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="true">
										<metaDataColonne>
											<nomcolonne>EG_FINESS_CATEGAGRETAB</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="categagretab/text()!=''">
													<xsl:value-of select="categagretab/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
								</extensionsStructure>
							</structure>
						</structures>
					</structure>
				</structures>
			</structure>
		</config>
	</xsl:template>
</xsl:stylesheet>
