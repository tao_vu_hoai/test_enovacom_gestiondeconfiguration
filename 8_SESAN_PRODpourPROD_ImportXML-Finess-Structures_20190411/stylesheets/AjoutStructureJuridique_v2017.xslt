<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" version="1.0"/>
	<!--	####################### 	-->	
	<!--	TEMPLATE ROW(RACINE) 		-->
	<!--	####################### 	-->

	<xsl:template match="config/structure/structures/structure/structures/structure">
		<!-- STRUCTURE -->
		<structure>
			<libelleComplet>
				<xsl:value-of select="concat(libelleComplet/text(),' (EJ)')"/>
			</libelleComplet>
			<libelleCourt>
				<xsl:value-of select="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='NUMEROFINESSJURI']/valeur/text()"/>
			</libelleCourt>
			<raisonSociale>
				<xsl:value-of select="concat(raisonSociale/text(),' (EJ)')"/>
			</raisonSociale>
			<enseigne>
				<xsl:value-of select="concat(enseigne/text(),' (EJ)')"/>
			</enseigne>
			<codeUnite>
				<xsl:value-of select="concat('1',extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='NUMEROFINESSJURI']/valeur/text())"/>
			</codeUnite>
			<nom_departement>
				<code>
					<xsl:value-of select="substring(extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='NUMEROFINESSJURI']/valeur/text(),1,2)"/>
				</code>
			</nom_departement>
			<numeroFiness>
				<xsl:value-of select="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='NUMEROFINESSJURI']/valeur/text()"/>
			</numeroFiness>
			<structureType>
				<xsl:attribute name="etatRubrique">
					<xsl:value-of select="'LECTURE'"/>
				</xsl:attribute>
				<xsl:attribute name="idRubrique">
					<xsl:value-of select="''"/>
				</xsl:attribute>	
				<code>
					<xsl:value-of select="'0001'"/>
				</code>
			</structureType>
			<extensionsStructure>
				<extensionStructure>
					<metaDataColonne>
						<nomcolonne>NUMEROFINESSJURI</nomcolonne>
					</metaDataColonne>
					<valeur>
						<xsl:value-of select="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='NUMEROFINESSJURI']/valeur/text()"/>
					</valeur>  
				</extensionStructure>
				<xsl:choose>
					<xsl:when test="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='DATEMAJ_STR']/valeur/text()!=''">
						<extensionStructure>
							<metaDataColonne>
								<nomcolonne>DATEMAJ_STR</nomcolonne>
							</metaDataColonne>
							<valeur>
								<xsl:value-of select="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='DATEMAJ_STR']/valeur/text()"/>
							</valeur>  
						</extensionStructure>
					</xsl:when>
					<xsl:when test="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='DATEMAJ_FINESS']/valeur/text()!=''">
						<extensionStructure>
							<metaDataColonne>
								<nomcolonne>DATEMAJ_FINESS</nomcolonne>
							</metaDataColonne>
							<valeur>
								<xsl:value-of select="extensionsStructure/extensionStructure[metaDataColonne/nomcolonne/text()='DATEMAJ_FINESS']/valeur/text()"/>
							</valeur>  
						</extensionStructure>
					</xsl:when>		
				</xsl:choose>
				<extensionStructure delete="true">
					<metaDataColonne>
						<nomcolonne>PAYS</nomcolonne>
					</metaDataColonne>
					<valeur>
						<xsl:value-of select="'99000'"/>
					</valeur>  
				</extensionStructure>
				<extensionStructure>
					<metaDataColonne>
						<nomcolonne>TYPE_STRUCTURE</nomcolonne>
					</metaDataColonne>
					<valeur>
						<xsl:value-of select="'0001'"/>
					</valeur>  
				</extensionStructure>
				<extensionStructure>
					<metaDataColonne>
						<nomcolonne>TYPO_STRUCTURE</nomcolonne>
					</metaDataColonne>
					<valeur>
						<xsl:value-of select="'1'"/>
					</valeur>  
				</extensionStructure>
			</extensionsStructure>	
		</structure>
	</xsl:template>
</xsl:stylesheet>

