<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" version="1.0"/>
	<!--	####################### 	-->
	<!--	RPPS  		-->
	<!--	REC110 passage � la V300	-->
	<!--	####################### 	-->
	
	<!-- INPUT -->
	<xsl:param name="DATEDESINFOS" select="DATEDESINFOS" />
	<!-- apostropheEnovacom -->
	<xsl:variable name="apostropheEnovacom" select='"&apos;"'/>
	<!-- CONFIG -->
	<xsl:template match="row">
		<!-- STRUCTURE -->
		<config>
			<xsl:attribute name="etatRubrique"><xsl:value-of select="'LECTURE'"/></xsl:attribute>
			<xsl:attribute name="libelle"><xsl:value-of select="'IDF_Sant�'"/></xsl:attribute>
			<structure>
				<xsl:attribute name="etatRubrique"><xsl:value-of select="'LECTURE'"/></xsl:attribute>
				<xsl:attribute name="idRubrique"><xsl:value-of select="'2'"/></xsl:attribute>
				<structures> 
					<structure>
						<xsl:choose>
							<xsl:when test="(NUMERO_FINESS_EJ/text()!='' and NUMERO_FINESS_ETABLISSEMENT!='')">
								<xsl:attribute name="etatRubrique"><xsl:value-of select="concat('1',NUMERO_FINESS_EJ/text())"/></xsl:attribute>
								<xsl:attribute name="idRubrique"><xsl:value-of select="concat('1',NUMERO_FINESS_EJ/text())"/></xsl:attribute>

							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="etatRubrique"><xsl:value-of select="Identifiant_tech_structure/text()"/></xsl:attribute>
								<xsl:attribute name="idRubrique"><xsl:value-of select="Identifiant_tech_structure/text()"/></xsl:attribute>
								<libelleComplet>
									<xsl:choose>
										<xsl:when test="Raison_sociale/text()!=''">
											<xsl:value-of select="Raison_sociale/text()"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="Enseigne_commerciale/text()"/>
										</xsl:otherwise>
									</xsl:choose>
								</libelleComplet>
								<libelleCourt>
									<xsl:value-of select="Identification_nationale_de_la_structure/text()"/>
								</libelleCourt>
								<codeUnite>
									<xsl:value-of select="Identifiant_tech_structure/text()"/>
								</codeUnite>
								<xsl:if test="Date_d_ouverture_structure/text()!=''">
									<dateOuverture>
										<xsl:value-of select="Date_d_ouverture_structure/text()"/>
									</dateOuverture>
								</xsl:if>
								<dateFermeture>
									<xsl:choose>
										<xsl:when test="DELTA/text()!='SUPPRESSION'">
											<xsl:value-of select="Date_de_fermeture_structure/text()"/>
										</xsl:when>
										<xsl:when test="DELTA/text()='SUPPRESSION'">
										<xsl:value-of select="$DATEDESINFOS"/>
										</xsl:when>
										</xsl:choose>
								</dateFermeture>
								
								<structureType>
									<xsl:attribute name="etatRubrique"><xsl:value-of select="'LECTURE'"/></xsl:attribute>
									<xsl:attribute name="idRubrique"><xsl:value-of select="''"/></xsl:attribute>
									<code>
										<xsl:choose>
											<xsl:when test="Num�ro_FINESS_EJ/text()!=''">
												<xsl:value-of select="'0001'"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="'0003'"/>
											</xsl:otherwise>
										</xsl:choose>
									</code>
								</structureType>
								<extensionsStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>IDNAT_STRUCT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Identification_nationale_de_la_structure/text()!=''">
													<xsl:value-of select="Identification_nationale_de_la_structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
										
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>STR_SOURCEALIMENTATION</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:value-of select="'RPPS'"/>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>TYPEIDNAT_STRUCT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="RPPS_rang/text()!=''">
													<xsl:value-of select="'4'"/>
												</xsl:when>
												<xsl:when test="ADELI_rang/text()!=''">
													<xsl:value-of select="'0'"/>
												</xsl:when>	
												<xsl:when test="string-length(Identification_nationale_de_la_structure/text())='15' and 																substring((Identification_nationale_de_la_structure/text()),1,1)='3'">
															<xsl:value-of select="'3'"/>
												</xsl:when>		
												<xsl:when test="string-length(Identification_nationale_de_la_structure/text())='15' and 
																substring((Identification_nationale_de_la_structure/text()),1,1)='4'">
															<xsl:value-of select="'4'"/>
												</xsl:when>
												<xsl:when test="string-length(Identification_nationale_de_la_structure/text())='10' and 
															substring((Identification_nationale_de_la_structure/text()),1,1)='2'">
																	<xsl:value-of select="'2'"/>
												</xsl:when>
												<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_IDENTIFIANTDELASTRUCTURE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Identification_nationale_de_la_structure/text()!=''">
													<xsl:value-of select="Identification_nationale_de_la_structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_COMPLEMENTDESTINATAIRE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Compl�ment_destinataire_coord._structure/text()!=''">
													<xsl:value-of select="Compl�ment_destinataire_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_COMPLEMENTPOINTGEOGRAPHIQUE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Compl�ment_point_g�ographique_coord._structure/text()!=''">
													<xsl:value-of select="Compl�ment_point_g�ographique_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_NUMEROVOIE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Num�ro_Voie_coord._structure/text()!=''">
													<xsl:value-of select="Num�ro_Voie_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_INDICEREPETITIONVOIE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Indice_r�p�tition_voie_coord._structure/text()!=''">
													<xsl:value-of select="Indice_r�p�tition_voie_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODETYPEDEVOIE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_type_de_voie_coord._structure/text()!=''">
													<xsl:value-of select="Code_type_de_voie_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLETYPEDEVOIE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_type_de_voie_coord._structure/text()!=''">
													<xsl:value-of select="Libell�_type_de_voie_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLEVOIE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_Voie_coord._structure/text()!=''">
													<xsl:value-of select="Libell�_Voie_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_MENTIONDISTRIBUTION(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Mention_distribution_coord._structure/text()!=''">
													<xsl:value-of select="Mention_distribution_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_BUREAUCEDEX(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Bureau_cedex_coord._structure/text()!=''">
													<xsl:value-of select="Bureau_cedex_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODEPOSTAL(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_postal_coord._structure/text()!=''">
													<xsl:value-of select="Code_postal_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODECOMMUNE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_commune_coord._structure/text()!=''">
													<xsl:value-of select="Code_commune_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLECOMMUNE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_commune_coord._structure/text()!=''">
													<xsl:value-of select="Libell�_commune_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODEPAYS(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_pays_coord._structure/text()!=''">
													<xsl:value-of select="Code_pays_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLEPAYS(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_pays_coord._structure/text()!=''">
													<xsl:value-of select="Libell�_pays_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_TELEPHONE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="T�l�phone_coord._structure/text()!=''">
													<xsl:value-of select="T�l�phone_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_TELEPHONE2(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="T�l�phone_2_coord._structure/text()!=''">
													<xsl:value-of select="T�l�phone_2_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_TELECOPIE(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="T�l�copie_coord._structure/text()!=''">
													<xsl:value-of select="T�l�copie_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_ADRESSEE-MAIL(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Adresse_e-mail_coord._structure/text()!=''">
													<xsl:value-of select="Adresse_e-mail_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_DATEDEMISEAJOUR(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Date_de_mise_�_jour_coord._structure/text()!=''">
													<xsl:value-of select="Date_de_mise_�_jour_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_DATEDEFIN(COORD_STRUCTURE)</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Date_de_fin_coord._structure/text()!=''">
													<xsl:value-of select="Date_de_fin_coord._structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_TYPEDIDENTIFIANTPM</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Type_d_identifiant_PM/text()!=''">
													<xsl:value-of select="Type_d_identifiant_PM/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_IDENTIFICATIONNATIONALEDELASTRUCTURE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Identification_nationale_de_la_structure/text()!=''">
													<xsl:value-of select="Identification_nationale_de_la_structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_NUMEROSIRET</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Num�ro_SIRET/text()!=''">
													<xsl:value-of select="Num�ro_SIRET/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_NUMEROSIREN</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Num�ro_SIREN/text()!=''">
													<xsl:value-of select="Num�ro_SIREN/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_NUMEROFINESSETABLISSEMENT</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Num�ro_FINESS_Etablissement/text()!=''">
													<xsl:value-of select="Num�ro_FINESS_Etablissement/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_NUMEROFINESSEJ</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Num�ro_FINESS_EJ/text()!=''">
													<xsl:value-of select="Num�ro_FINESS_EJ/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_RPPSRANG</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="RPPS_rang/text()!=''">
													<xsl:value-of select="RPPS_rang/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_ADELIRANG</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="ADELI_rang/text()!=''">
													<xsl:value-of select="ADELI_rang/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_NUMEROLICENCEOFFICINE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Num�ro_licence_officine/text()!=''">
													<xsl:value-of select="Num�ro_licence_officine/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_DATEDOUVERTURESTRUCTURE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Date_d_ouverture_structure/text()!=''">
													<xsl:value-of select="Date_d_ouverture_structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
										<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_DATEDEFERMETURESTRUCTURE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Date_de_fermeture_structure/text()!=''">
													<xsl:value-of select="Date_de_fermeture_structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
										<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_DATEDEMISEAJOURSTRUCTURE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Date_de_mise_�_jour_structure/text()!=''">
													<xsl:value-of select="Date_de_mise_�_jour_structure/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODEAPE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_APE/text()!=''">
													<xsl:value-of select="Code_APE/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLEAPE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_APE/text()!=''">
													<xsl:value-of select="Libell�_APE/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
										<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODECATEGORIEJURIDIQUE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_cat�gorie_juridique/text()!=''">
													<xsl:value-of select="Code_cat�gorie_juridique/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
											<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLECATEGORIEJURIDIQUE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_cat�gorie_juridique/text()!=''">
													<xsl:value-of select="Libell�_cat�gorie_juridique/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
												<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_CODESECTEURDACTIVITE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Code_secteur_d_activit�/text()!=''">
													<xsl:value-of select="Code_secteur_d_activit�/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
												<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LIBELLESECTEURDACTIVITE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Libell�_secteur_d_activit�/text()!=''">
													<xsl:value-of select="Libell�_secteur_d_activit�/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
										<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_RAISONSOCIALE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Raison_sociale/text()!=''">
													<xsl:value-of select="Raison_sociale/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_ENSEIGNECOMMERCIALE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="Enseigne_commerciale/text()!=''">
													<xsl:value-of select="Enseigne_commerciale/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LATITUDE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="LATITUDE/text()!=''">
													<xsl:value-of select="LATITUDE/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_LONGITUDE</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="LONGITUDE/text()!=''">
													<xsl:value-of select="LONGITUDE/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_PRECISION</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="PRECISION/text()!=''">
													<xsl:value-of select="PRECISION/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
									<extensionStructure delete="false">
										<metaDataColonne>
											<nomcolonne>RPPS_TYPE_DE_PRECISION</nomcolonne>
										</metaDataColonne>
										<valeur>
											<xsl:choose>
												<xsl:when test="TYPE_PRECISION/text()!=''">
													<xsl:value-of select="TYPE_PRECISION/text()"/>
												</xsl:when>
											</xsl:choose>
										</valeur>
									</extensionStructure>
								</extensionsStructure>
							</xsl:otherwise>
						</xsl:choose>
					</structure>
				</structures>
			</structure>
		</config>
	</xsl:template>
</xsl:stylesheet>
